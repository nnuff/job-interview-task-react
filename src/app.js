import './styles/styles.scss';

import React from 'react';
import {render} from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './redux/rootStore';

import MyApp from './components/myApp';

const app = document.getElementById('app');

render( 
<Provider store={configureStore()}>
    <MyApp />
</Provider>, app);
