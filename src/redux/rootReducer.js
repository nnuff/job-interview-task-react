import { combineReducers } from 'redux';
import photos from './gallery/photosReducer';

export default combineReducers({
    photos
});