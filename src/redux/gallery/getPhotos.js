import {getPhotosPending, getPhotosSuccess, getPhotosError} from './photosActions';

const fetchData = (limit = 20) => dispatch => {
        dispatch(getPhotosPending());
        fetch(`https://jsonplaceholder.typicode.com/photos?_limit=${limit}`)
        .then(res => res.json())
        .then(res => {
            dispatch(getPhotosSuccess(res));
            return res;
        }).catch(error => {
            dispatch(getPhotosError(error))
            throw Error("There was a problem");
        })
}

export default fetchData;