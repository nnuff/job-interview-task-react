// Get Photos
export const GET_PHOTOS_PENDING = 'GET_PHOTOS_PENDING';
export const GET_PHOTOS_SUCCESS = 'GET_PHOTOS_SUCCESS';
export const GET_PHOTOS_ERROR = 'GET_PHOTOS_ERROR';

export const getPhotosPending = () => ({
    type: GET_PHOTOS_PENDING
});

export const getPhotosSuccess = photos => ({
    type: GET_PHOTOS_SUCCESS,
    payload: photos
});

export const getPhotosError = error => ({
    type: GET_PHOTOS_ERROR,
    error: error
});