import {GET_PHOTOS_PENDING, GET_PHOTOS_SUCCESS, GET_PHOTOS_ERROR} from './photosActions';

const initialState = {
    photos: [],
    pending: false,
    error: null
};

export default (state = initialState, action) => {
    switch(action.type) {
        case GET_PHOTOS_PENDING:
            return {
                ...state,
                pending: true
            }
        case GET_PHOTOS_SUCCESS:
            return {
                ...state,
                pending: false,
                photos: action.payload
            }
        case GET_PHOTOS_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        default:
            return state;
    }
}


export const getPhotos = state => state.photos;
export const getPhotosPending = state => state.pending;
export const getPhotosError = state => state.error;
