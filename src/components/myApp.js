import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import fetchDataAction from '../redux/gallery/getPhotos';
import { getPhotosError, getPhotos, getPhotosPending } from '../redux/gallery/photosReducer';

import Header from './Header';
import Nav from './Nav';
import Gallery from './Gallery';
import Footer from './Footer';

class myApp extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        const {getPhotos} = this.props;
        getPhotos();
    }

    handleSetLimit = (number) => {
        const {getPhotos} = this.props;
        getPhotos(number);   
    }

    render() {
        const {photos, error, pending} = this.props.photos;
        const subtitle = 'Lorem ipsum dolor remi';

        return (
            <div>
                <Header subtitle={subtitle} />
                <div className="content">
                    <Nav default={20} limits={[20, 50, 100]} handleSetLimit={this.handleSetLimit}/>
                    {error && <div>{error}</div>}
                    {pending && <div className="heading_1 f-primary-color text-centered">Loading...</div>}
                    {!pending && <Gallery photos={photos} />}
                </div>
                <Footer />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    error: getPhotosError(state),
    photos: getPhotos(state),
    pending: getPhotosPending(state)
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getPhotos: fetchDataAction
}, dispatch);


export default connect(
    mapStateToProps,
    mapDispatchToProps
) (myApp);