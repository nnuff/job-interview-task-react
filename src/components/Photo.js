import React from 'react';

const Photo = props => {
    return (
        <div className="col col-4">
            <div className="card mb-m shadow">
                <img className="card-image" src={props.url} alt="..." />
                <div className="card-body">
                    <p className="card-title">{props.title}</p>
                </div>
            </div>
        </div>

    )
}

export default Photo;