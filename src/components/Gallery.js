import React from 'react';
import Photo from './Photo';
import LazyLoad from 'react-lazyload';

const Photos = props => {
    return (
        <div className="container">
            <div className="row">
            {props.photos.map((photo) => (
                <LazyLoad key={photo.id} placeholder={
                    <div className="col col-4 text-centered">
                        <div className="lds-dual-ring"></div>
                    </div>
                }>
                <Photo
                key={photo.id}
                title={photo.title} 
                url={photo.url}
                thumbUrl={photo.thumbnailUrl}
                />
                </LazyLoad>
            ))
            }
            </div>
        </div>
    )
}

export default Photos;