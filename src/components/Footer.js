import React from 'react';

const Footer = (props) => {
    return (
        <div className="footer">
            <div className="container text-centered">
                <p className="footer__text">{props.copyrights}</p>
            </div>

        </div>
    )
};

Footer.defaultProps = {
    copyrights: 'Copyrights © 2019'
  };
  
export default Footer;