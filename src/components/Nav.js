import React, {Component} from 'react';

export default class Nav extends Component {
    state = {
        key: this.props.default
    }

    handleSetLimit = (e) => {
        e.preventDefault();
        const key = e.target.dataset["limit"];
        this.props.handleSetLimit(key);

        this.setState(() => ({ key }));
    }

    render() {
        return (
            <div className="container">
                <ul className="pagination justify-content-center">
                    <li className="page-item">
                        <span className="page-label">SHOW</span>
                    </li>
                    {this.props.limits.map((limit) => (
                        <li className="page-item" key={limit}>
                            <a 
                            className={"page-link " + (limit == this.state.key ? "page-link-active" : "page-link-inactive")} 
                            href="" 
                            data-limit={limit} 
                            onClick={this.handleSetLimit}>
                                {limit}
                            </a>
                        </li>
                    ))}
                </ul>
            </div>
        )
    }


}
