const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
module.exports = () => {
  const extractPlugin = new ExtractTextPlugin('./assets/css/styles.css');
  return {
    entry: './src/app.js',
    output: {
      path: path.join(__dirname, 'dist'),
      filename: 'bundle.js',
      publicPath: '/'
    },
    module: {
      rules: [{
        loader: 'babel-loader',
        test: /\.js$/,
        exclude: /node_modules/
      },{
        test: /\.scss$/,
        include: [path.resolve(__dirname, 'src', 'styles')],
        use: extractPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
               // sourceMap: true
              }
            },
            {
              loader: 'sass-loader',
              options: {
               // sourceMap: true
              }
            }
          ],
          fallback: 'style-loader'
        })
      }]
    },
    plugins: [extractPlugin],
    devtool: 'cheap-module-eval-source-map',
    devServer: {
      contentBase: path.join(__dirname, 'dist'),
      open: true,
      openPage: ''
    }
  }
};
