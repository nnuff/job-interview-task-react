## Fetching data from API using React/Redux

---
Small Gallery build with React and Redux

1. Data is retrieved from [JSONPlaceholder](https://jsonplaceholder.typicode.com/)
2. Show: 20, 40, 100 Photos

## How to run
Please run below commands in your terminal:

1. 'yarn install'  
2. 'yarn dev-server' - launches devServer and opens automatically a new window tab with url: http://localhost:8080/
